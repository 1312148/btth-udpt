Omniauth-Facebook and Koala đây là 2 gem hỗ trợ cho việc đăng nhập kết nối đến facebok và lấy dữ liệu từ facebook.
Omniauth-Facebook là một Rack middleware hỗ trợ để xác thực những request từ người dung để lấy dữ liệu từ Facebook. 
Thêm middleware vào Rails app:  config/initializers/omniauth.rb


![](https://lh6.googleusercontent.com/O4Sw-IBS7Yi6n70Q1zyuw2j-hh2yKge4KvGBER5gymBxVWRUpYmCkw83f8wcklCONmjzAHtweVwPFus=w1366-h651)



-provider :facebook, ENV['app_id'], ENV['secret'] gửi xác thực các request đến một app được tạo trên  https://developers.facebook.com/ 
với app_id và secret sau đó trả về access token để Koala lấy dữ liệu.
-scope: 'user_about_me, user_friends' xác định các hành động có thể được thực hiện với tư cách một user cho phép lấy được thông tin của user 
như user_about_me (tên, ảnh đại diện,…) và user_friends (danh sách bạn bè, số lượng bạn,…)
Koala là một thư viện facebook cho ruby hỗ trợ facebook GRAPH API, REST API, cập nhật thời gian thực, người sử dụng kiểm tra, xác nhận và OAuth.



![](https://lh4.googleusercontent.com/oLc18F0Ode7wNu6KIi7cV30DM3qV0yQpDJi_AMu3_Twpgw4Szx7uZUiiaZRZHIu4I2uw_beghe9_28Y=w1366-h651)


access_token = auth['token'] lấy chuỗi token từ omniauth.
facebook=Koala::Facebook::API.new(access_token) 
facebook.get_object("me?fields=name,picture")
Lấy ra chuỗi JSON thông tin của user bao gồm tên và hình ảnh
facebook.get_object("me?fields=friends")
Lấy được chuỗi JSON về list friend bao gồm : data[], paging và summary.
Kết hợp với việc sử dụng Graph API  Explorer. Đây là một kiểu đồ thị cho phép bạn lấy được mọi thông tin trên Facebook. Nó bao gồm:
   -node (nút): Những thứ xuất hiện trên facebook có tương tác như ảnh, post…
   -edge (cạnh): những kết nối, liên hệ giữa nó.
   -field (trường/lĩnh vực): thông tin của nó.
Với câu lệnh me?fields= name,picture  thì graph api sẽ trả kết quả:


![](https://lh6.googleusercontent.com/THZ2Uiidf38qPxlOlUek52J5CZ30nWhIBKOa6HTK3b3LPiN077seFDAhPRsOi4yazguYYVqvtBCiXFY=w1366-h651-rw)


me?fields=friends:


![](https://lh4.googleusercontent.com/IP7nDd9y0MHWJXrGECDqk2pYUcoN6rR-L8-cAefsdU0zq236_P1zucptYx_TGyQ7GNWAiyWwJT4oPZM=w1366-h651-rw)


Sau khi đã lấy được các chuỗi JSON thì gán vào các biến ở Usercontroller đễ hiễn thị ra views


![](https://lh5.googleusercontent.com/qTBnyhUFiU2HwIuRcwxWEg81Y32P2iaU4C6VrQtp9Dj4l2fw45dR_MCWAag2MMCw6Iex-M6NQIaLMGY=w1366-h651)


Ở trang login.html.erb gọi lại các biến đã tạo trong controller để hiễn thị kết quả ra trang web


![](https://lh4.googleusercontent.com/gQGekXxFLWUM9w5aNNC6DqnPtJmXRzEOQsOjGxq-PTZFjUR-vUqg3OwLbz_E_Q0EIP08jxJ2fU8u_jc=w1366-h651)


Để kết nối tới app đã tạo trên https://developers.facebook.com/ cần phải cấu hình lại route.rb


![](https://lh6.googleusercontent.com/bW4gUYQ8l3FcTBY8-vrp62WqS9mx2doSxmNW3Y8M_J5V24sJsBIJr2JJtoGHaiXgA6Who434oNaaEHE=w1366-h651)


Khi có một request đến http:// localhost:3000/auth/facebook thì nó sẽ gọi  đến provider trong middleware kiểm tra app_idd và secret


![](https://lh5.googleusercontent.com/WdFuTtcfgZ1OBqA2c0xS5gONplFRt5Tijm9oOVjqXOIFMKMfoSPLP_vReu_fBXfBjbkWgt3-Diysy7E=w1366-h651-rw)


nếu đúng thì sẽ callback gọi đến các biến trong def login ở Usercontroller để hiển thị những kết quả ra 
http:// localhost:3000/auth/facebook/callback....


![](https://lh6.googleusercontent.com/kLaepDDWy0tNmsvirivUEmaSSxSnxBQNonqKCnmlfzHSsZhFLfZ3up-Mxu2jYeq1-WTy8AMwXTXM4uM=w1366-h651-rw)




